job "network-internal-proxy" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  priority = 90

  vault {
    policies = [
      "api-lets-encrypt",
      "api-cloudflare",
      "job-network-internal-proxy"
    ]
  }

  constraint {
    attribute = "${meta.service_proxy}"
    value = "internal"
  }
  
  constraint {
    distinct_hosts = true
  }

  group "proxima" {
    count = 2

    volume "traefik-acme" {
      type = "host"
      read_only = false
      source = "traefik-acme"
    }

    network {
      mode = "bridge"

      port "http" {
        static = "80"
        to = "80"
      }

      port "althttp" {
        static = "8008"
        to = "8008"
      }

      port "https" {
        static = "443"
        to = "443"
      }

      port "althttps" {
        static = "8443"
        to = "8443"
      }

      port "dns" {
        static = "53"
        to = "53"
      }

      port "tlsdns" {
        static = "853"
        to = "853"
      }

      port "dashboard" {
        static = "8081"
        to = "8081"
      }
    }

    service {
      name = "internal-proxy"
      port = "${NOMAD_PORT_dashboard}"
      task = "traefik"

      connect {
        native = true
      }
    }

    task "traefik" {
      driver = "docker"

      volume_mount {
        volume = "traefik-acme"
        destination = "/acme"
        read_only = false
      }

      resources {
        cpu = 500
        memory = 300
      }

      config {
        image = "[[ .traefikImage ]]"

        ports = [
          "http",
          "https",
          "althttp",
          "althttps",
          "dashboard",
          "dns",
          "tlsdns"
        ]

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "local/static-http.toml:/etc/traefik/static-http.toml"
        ]
      }

      template {
        data = <<EOH
{{ with secret "api/cloudflare/zones/carboncollins.se" }}
CF_DNS_API_TOKEN="{{ index .Data.data "token" }}"
{{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env = true
      }

      template {
        data = <<EOH
[[ fileContents "./config/static-http.template.toml" ]]
        EOH

        destination = "local/static-http.toml"
        change_mode = "noop"
      }

      template {
        data = <<EOH
[[ fileContents "./config/traefik.template.toml" ]]
        EOH

        destination = "local/traefik.toml"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
